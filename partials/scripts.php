<!-- Sweet Alert-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- Contact JS -->
<script type="text/javascript" src="script/contact.js"></script>
<!-- jQuery (necessary for JavaScript plugins) -->
<script type="text/javascript" src="script/jquery.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="script/bootstrap.min.js"></script>
<script type="text/javascript" src="script/slick.slider.min.js"></script>
<script type="text/javascript" src="script/jquery.countdown.min.js"></script>
<script type="text/javascript" src="script/fancybox.pack.js"></script>
<script type="text/javascript" src="script/isotope.min.js"></script>
<script type="text/javascript" src="script/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="build/mediaelement-and-player.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="script/functions.min.js"></script>
<script type="text/javascript" src="script/dynamic-fields-cars.js"></script>
<script type="text/javascript" src="script/dynamic-fields-bikes.js"></script>