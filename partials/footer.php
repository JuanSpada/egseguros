<!--// Footer \\-->
<footer id="automobile-footer" class="automobile-footer-one">

<!--// Footer Widget \\-->
<div class="automobile-footer-widget">
    <div class="container">
        <div class="row d-flex justify-content-between p-3">
            <div class="ssn col-md-6">
                <a href="https://www.argentina.gob.ar/superintendencia-de-seguros">
                    <span>
                        <img src="images/SSN.png" style="width: 150px;">
                    </span>
                </a>
                <p>Atención al asegurado: 0800-666-8400 <br> Organismo de control - 
                    <a href="https://www.argentina.gob.ar/ssn" style="text-decoration: underline">www.argentina.gob.ar/ssn</a>
                </p>
            </div>
            <div class="col-md-6 automobile-copyright py-4"> 
                <span>Todos los Derechos Reservados. Clever Broker © Copyright 2019.</p> 
            </div>
        </div>
    </div>
</div>
<!--// Footer Widget \\-->

</footer>
<!--// Footer \\-->