<form class="car-form" action="contact.php" method="POST">
    <ul>
        <li>
            <label>Nombre y Apellido:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="name" required>
            </div>
        </li>
        <li>
            <label>Email:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="email" required>
            </div>
        </li>
        <li>
            <label>Fecha de Nacimiento:</label>
            <div class="automobile-banner-submit">
                <input type="date" name="birthdate" required>
            </div>
        </li>

        <li>
            <ul>
                <li><label>Año:</label>
                    <div class="automobile-banner-select">
                        <select name="year" required>
                            <option value="2019">2019</option>
                            <option value="2018">2018</option>
                            <option value="2017">2017</option>
                            <option value="2016">2016</option>
                            <option value="2015">2015</option>
                            <option value="2014">2014</option>
                            <option value="2013">2013</option>
                            <option value="2012">2012</option>
                            <option value="2011">2011</option>
                            <option value="2010">2010</option>
                            <option value="2009">2009</option>
                            <option value="2008">2008</option>
                            <option value="2007">2007</option>
                            <option value="2006">2006</option>
                            <option value="2005">2005</option>
                            <option value="2004">2004</option>
                            <option value="2003">2003</option>
                            <option value="2002">2002</option>
                            <option value="2001">2001</option>
                            <option value="2000">2000</option>
                            <option value="1999">1999</option>
                            <option value="1998">1998</option>
                            <option value="1997">1997</option>
                            <option value="1996">1996</option>
                            <option value="1995">1995</option>
                        </select>
                    </div>
                </li>
                <li>
                    <label>Cobertura:</label>
                    <div class="automobile-banner-select">
                        <select required name="insurance">
                            <option selected disabled>Seleccionar Cobertura</option>
                            <option value="0">Todas</option>
                            <option value="1">Básicas</option>
                            <option value="2">Terceros completos</option>
                            <option value="2">Todo riesgo</option>

                        </select>
                    </div>
                </li>
            </ul>
        </li>

        <li>
            <div id="vehicle">
            <label>Marca:</label>
                <div class="form-group">
                    <div class="automobile-banner-select">
                        <select name="brand" class="formmake-car" required>
                            <option value="" selected="selected">Marca</option>
                        </select>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <label>Modelo:</label>
            <div class="form-group">
                <div class="automobile-banner-select">
                    <select name="model" class="formmodel-car" required>
                        <option value="" selected="selected">Modelo</option>
                    </select>
                </div>
            </div>
        </li>
    
        <li>
            <label>Localidad:</label>
            <div class="automobile-banner-select">
                <select name="location"required>
                    <option selected disabled>Localidad</option>
                    <?php foreach($localidades as $localidad) : ?>
                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </li>
        <li>
            <div class="banner-check">
                <input type="checkbox" name="gnc">
                <laber>GNC</label>
            </div>
        </li>
        <li class="banner-full-form">
            <label class="banner-submit"><i class="icon-search"></i><input type="submit" value="Cotizar"></label>
        </li>
    </ul>
</form>