
<!DOCTYPE html>
<html lang="en">
    <?php include('partials/head.php'); ?>
  <body>
	
    <!--// Main Wrapper \\-->
    <div class="automobile-main-wrapper">

        <?php include ('partials/header.php'); ?>

        
		<div class="automobile-banner">

            <!--// Slider \\-->
            <div class="automobile-banner-layer">
                <img src="extra-images/foto-1.jpg" alt="">
                <span class="blue-transparent"></span>
                <div class="automobile-banner-caption">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-6">
                                <div class="automobile-banner-text automobile-banner-text3">
                                    <span>Tu Seguro </span>
                                    <h1>rápido y fácil</h1>
                                    <ul>
                                        <li><i class="icon-check"></i>100% Digital</li>
                                        <li><i class="icon-check"></i>Contamos con las mejores compañias</li>
                                        <li><i class="icon-check"></i>Asesoramiento inmediato por WhatsApp</li>
                                    </ul>
                                    <!-- <a href="#nosotros" class="automobile-banner-btn automobile-bgcolor">Más Info</a> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="automobile-banner-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#banner2" aria-controls="banner2" role="tab" data-toggle="tab">Autos</a></li>
                                        <li role="presentation" class="radius"><a href="#banner1" aria-controls="banner1" role="tab" data-toggle="tab">Motos</a></li>
                                      
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane" id="banner1">
                                            <div class="automobile-banner-form">
                                                <form id="form" action="contact.php" method="POST">
                                                    <ul>
                                                        <li>
                                                            <label>Nombre y Apellido:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="name" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Email:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="email" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Teléfono:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="phone" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Edad:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="birthdate" required> 
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div id="vehicle">
                                                            <label>Marca:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="brand" class="formmake-bike" required>
                                                                            <option value="" selected="selected">Marca</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Modelo:</label>
                                                            <div class="form-group">
                                                                <div class="automobile-banner-select" >
                                                                    <select name="model" class="formmodel-bike" required>
                                                                        <option value="" selected="selected">Modelo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Año:</label>
                                                            <div class="automobile-banner-select">
                                                                <select required name="year">
                                                                    <option selected disabled>Año</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1995">1995</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Localidad:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="location" required>
                                                                    <option selected disable>Localidad</option>
                                                                    <?php foreach($localidades as $localidad) : ?>
                                                                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="banner-full-form">
                                                            <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="bike-1" value="Cotizar"></label>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                        <!-- FORM AUTOS -->
                                        <div role="tabpanel" class="tab-pane active" id="banner2">
                                            <div class="automobile-banner-form">
                                            <form id="form" action="contact.php" method="POST">
                                                    <ul>
                                                        <li>
                                                            <label>Nombre y Apellido:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="name" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Email:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="email" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Teléfono:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="phone" required>
                                                            </div>
                                                        </li>                                                         
                                                        <li>
                                                            <label>Edad:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="birthdate" required>
                                                            </div>
                                                        </li>


                                                        <li>
                                                            <div id="vehicle">
                                                            <label>Marca:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="brand" class="formmake-car" required>
                                                                            <option value="" selected="selected">Marca</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Modelo:</label>
                                                            <div class="form-group">
                                                                <div class="automobile-banner-select">
                                                                    <select name="model" class="formmodel-car" required>
                                                                        <option value="" selected="selected">Modelo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Año:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="year" required>
                                                                    <option disabled selected>Año</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1995">1995</option>
                                                                </select>
                                                            </div>  
                                                        </li>
                                                    
                                                        <li>
                                                            <label>Localidad:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="location"required>
                                                                    <option selected disabled>Localidad</option>
                                                                    <?php foreach($localidades as $localidad) : ?>
                                                                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="banner-full-form">
                                                            <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="car-1" value="Cotizar"></label>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="automobile-banner-layer" id='slide-2'>
                <img src="extra-images/fondo-3.jpeg" alt="">
                <span class="blue-transparent"></span>
                <div class="automobile-banner-caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="automobile-banner-text automobile-banner-text3">
                                    <span>Podes Ahorrar</span> </span>
                                    <h1>hasta un 30%</h1>
                                    <ul>
                                        <li><i class="icon-check"></i>Trabajamos con todos los medios de pago</li>
                                        <li><i class="icon-check"></i>Cotizá y Ahorrá</li>
                                        <!-- <li><i class="icon-check"></i>Asesoramiento inmediato por Whats App</li> -->
                                    </ul>
                                    <!-- <a href="#nosotros" class="automobile-banner-btn automobile-bgcolor">Más Info</a> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="automobile-banner-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav-tabs" role="tablist">
                                      <li role="presentation" class="active"><a href="#banner3" aria-controls="banner3" role="tab" data-toggle="tab">Autos</a></li>
                                      <li role="presentation" class="radius"><a href="#banner4" aria-controls="banner4" role="tab" data-toggle="tab">Motos</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="banner3">
                                            <div class="automobile-banner-form">
                                                <form id="form" action="contact.php" method="POST">
                                                    <ul>
                                                        <li>
                                                            <label>Nombre y Apellido:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="name" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Email:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="email" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Teléfono:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="phone" required>
                                                            </div>
                                                        </li>                                                         
                                                        <li>
                                                            <label>Edad:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="birthdate" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div id="vehicle">
                                                            <label>Marca:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="brand" class="formmake-car" required>
                                                                            <option value="" selected="selected">Marca</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Modelo:</label>
                                                            <div class="form-group">
                                                                <div class="automobile-banner-select">
                                                                    <select name="model" class="formmodel-car" required>
                                                                        <option value="" selected="selected">Modelo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    
                                                        <li>
                                                        <label>Año:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="year" required>
                                                                    <option disabled selected>Año</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1995">1995</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Localidad:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="location"required>
                                                                    <option selected disabled>Localidad</option>
                                                                    <?php foreach($localidades as $localidad) : ?>
                                                                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="banner-full-form">
                                                            <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="car-2" value="Cotizar"></label>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="banner4">
                                            <div class="automobile-banner-form">
                                                <form id="form" action="contact.php" method="POST">
                                                    <ul>
                                                        <li>
                                                            <label>Nombre y Apellido:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="name" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Email:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="email" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Teléfono:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="phone" required>
                                                            </div>
                                                        </li>                                                            
                                                        <li>
                                                            <label>Edad:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="birthdate" required> 
                                                            </div>
                                                    </li>
                                                        <li>
                                                            <div id="vehicle">
                                                            <label>Marca:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="brand" class="formmake-bike" required>
                                                                            <option value="" selected="selected">Marca</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Modelo:</label>
                                                            <div class="form-group">
                                                                <div class="automobile-banner-select" >
                                                                    <select name="model" class="formmodel-bike" required>
                                                                        <option value="" selected="selected">Modelo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        
                                                        <li><label>Año:</label>
                                                            <div class="automobile-banner-select">
                                                                <select required name="year">
                                                                    <option selected disabled>Año</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1995">1995</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                    
                                                        <li>
                                                            <label>Localidad:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="location" required>
                                                                    <option selected disable>Localidad</option>
                                                                    <?php foreach($localidades as $localidad) : ?>
                                                                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="banner-full-form">
                                                            <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="bike-2" value="Cotizar"></label>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="automobile-banner-layer" id="slide-3">
                <img src="extra-images/foto-3.jpg" alt="">
                <span class="blue-transparent"></span>
                <div class="automobile-banner-caption">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="automobile-banner-text automobile-banner-text3">
                                    <span>Te damos</span> </span>
                                    <h1>Asesoramiento 24/7</h1>
                                    <ul>
                                        <li><i class="icon-check"></i>Dudas, consultas? Estamos para ayudarte</li>
                                        <li><i class="icon-check"></i>Contamos con asistencia las 24hs</li>
                                        <li><i class="icon-check"></i>Escribinos a nuestro WhatsApp y te contestamos al instante</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="automobile-banner-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav-tabs" role="tablist">
                                      <li role="presentation" class="active"><a href="#banner5" aria-controls="banner5" role="tab" data-toggle="tab">Autos</a></li>
                                      <li role="presentation" class="radius"><a href="#banner6" aria-controls="banner6" role="tab" data-toggle="tab">Motos</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="banner5">
                                            <div class="automobile-banner-form">
                                                <form id="form" action="contact.php" method="POST">
                                                    <ul>
                                                        <li>
                                                            <label>Nombre y Apellido:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="name" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Email:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="email" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Teléfono:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="phone" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Edad:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="birthdate" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div id="vehicle">
                                                            <label>Marca:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="brand" class="formmake-car" required>
                                                                            <option value="" selected="selected">Marca</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Modelo:</label>
                                                            <div class="form-group">
                                                                <div class="automobile-banner-select">
                                                                    <select name="model" class="formmodel-car" required>
                                                                        <option value="" selected="selected">Modelo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    
                                                        <li><label>Año:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="year" required>
                                                                    <option disabled selected>Año</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1995">1995</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Localidad:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="location"required>
                                                                    <option selected disabled>Localidad</option>
                                                                    <?php foreach($localidades as $localidad) : ?>
                                                                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        
                                                        <li class="banner-full-form">
                                                            <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="car-3" value="Cotizar"></label>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="banner6">
                                            <div class="automobile-banner-form">
                                                <form id="form" action="contact.php" method="POST">
                                                    <ul>
                                                        <li>
                                                            <label>Nombre y Apellido:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="name" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Email:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="email" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Teléfono:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="phone" required>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Edad:</label>
                                                            <div class="automobile-banner-submit">
                                                                <input type="text" name="birthdate" required> 
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div id="vehicle">
                                                            <label>Marca:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="brand" class="formmake-bike" required>
                                                                            <option value="" selected="selected">Marca</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Modelo:</label>
                                                            <div class="form-group">
                                                                <div class="automobile-banner-select" >
                                                                    <select name="model" class="formmodel-bike" required>
                                                                        <option value="" selected="selected">Modelo</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        
                                                        <li><label>Año:</label>
                                                            <div class="automobile-banner-select">
                                                                <select required name="year">
                                                                    <option selected disabled>Año</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1995">1995</option>
                                                                </select>
                                                            </div>
                                                        </li>
                                                    
                                                        <li>
                                                            <label>Localidad:</label>
                                                            <div class="automobile-banner-select">
                                                                <select name="location" required>
                                                                    <option selected disable>Localidad</option>
                                                                    <?php foreach($localidades as $localidad) : ?>
                                                                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        </li>
                                                        <li class="banner-full-form">
                                                            <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="bike-3" value="Cotizar"></label>
                                                        </li>
                                                    </ul>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="automobile-banner-layer" id="slide-4">
                    <img src="extra-images/slider-4.jpg" alt="">
                    <span class="blue-transparent"></span>
                    <div class="automobile-banner-caption">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="automobile-banner-text automobile-banner-text3">
                                        <span>Somos como vos </span>
                                        <h1>Somos concientes</h1>
                                        <ul>
                                            <li><i class="icon-check"></i>Nuestra poliza es 100% digital</li>
                                            <li><i class="icon-check"></i>Juntos hacemos la diferencia</li>
                                            <!-- <li><i class="icon-check"></i>Escribinos a nuestro whats app y te contes amos al instante</li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="automobile-banner-tabs">
                                        <!-- Nav tabs -->
                                        <ul class="nav-tabs" role="tablist">
                                          <li role="presentation" class="active"><a href="#banner7" aria-controls="banner7" role="tab" data-toggle="tab">Autos</a></li>
                                          <li role="presentation" class="radius"><a href="#banner8" aria-controls="banner8" role="tab" data-toggle="tab">Motos</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="banner7">
                                                <div class="automobile-banner-form">
                                                    <form id="form" action="contact.php" method="POST">
                                                        <ul>
                                                            <li>
                                                                <label>Nombre y Apellido:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="name" required>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Email:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="email" required>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Teléfono:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="phone" required>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Edad:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="birthdate" required>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div id="vehicle">
                                                                <label>Marca:</label>
                                                                    <div class="form-group">
                                                                        <div class="automobile-banner-select">
                                                                            <select name="brand" class="formmake-car" required>
                                                                                <option value="" selected="selected">Marca</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Modelo:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select">
                                                                        <select name="model" class="formmodel-car" required>
                                                                            <option value="" selected="selected">Modelo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li><label>Año:</label>
                                                                <div class="automobile-banner-select">
                                                                    <select name="year" required>
                                                                        <option disabled selected>Año</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2014">2014</option>
                                                                        <option value="2013">2013</option>
                                                                        <option value="2012">2012</option>
                                                                        <option value="2011">2011</option>
                                                                        <option value="2010">2010</option>
                                                                        <option value="2009">2009</option>
                                                                        <option value="2008">2008</option>
                                                                        <option value="2007">2007</option>
                                                                        <option value="2006">2006</option>
                                                                        <option value="2005">2005</option>
                                                                        <option value="2004">2004</option>
                                                                        <option value="2003">2003</option>
                                                                        <option value="2002">2002</option>
                                                                        <option value="2001">2001</option>
                                                                        <option value="2000">2000</option>
                                                                        <option value="1999">1999</option>
                                                                        <option value="1998">1998</option>
                                                                        <option value="1997">1997</option>
                                                                        <option value="1996">1996</option>
                                                                        <option value="1995">1995</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        
                                                            <li>
                                                                <label>Localidad:</label>
                                                                <div class="automobile-banner-select">
                                                                    <select name="location"required>
                                                                        <option selected disabled>Localidad</option>
                                                                        <?php foreach($localidades as $localidad) : ?>
                                                                            <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                            
                                                            <li class="banner-full-form">
                                                                <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="car-4" value="Cotizar"></label>
                                                            </li>
                                                        </ul>
                                                    </form>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="banner8">
                                                <div class="automobile-banner-form">
                                                    <form id="form" action="contact.php" method="POST">
                                                        <ul>
                                                            <li>
                                                                <label>Nombre y Apellido:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="name" required>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Email:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="email" required>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Teléfono:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="phone" required>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Edad:</label>
                                                                <div class="automobile-banner-submit">
                                                                    <input type="text" name="birthdate" required> 
                                                                </div>
                                                            </li>
                                                            
                                                            <li>
                                                                <div id="vehicle">
                                                                <label>Marca:</label>
                                                                    <div class="form-group">
                                                                        <div class="automobile-banner-select">
                                                                            <select name="brand" class="formmake-bike" required>
                                                                                <option value="" selected="selected">Marca</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <label>Modelo:</label>
                                                                <div class="form-group">
                                                                    <div class="automobile-banner-select" >
                                                                        <select name="model" class="formmodel-bike" required>
                                                                            <option value="" selected="selected">Modelo</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li><label>Año:</label>
                                                                <div class="automobile-banner-select">
                                                                    <select required name="year">
                                                                        <option disabled selected>Año</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2014">2014</option>
                                                                        <option value="2013">2013</option>
                                                                        <option value="2012">2012</option>
                                                                        <option value="2011">2011</option>
                                                                        <option value="2010">2010</option>
                                                                        <option value="2009">2009</option>
                                                                        <option value="2008">2008</option>
                                                                        <option value="2007">2007</option>
                                                                        <option value="2006">2006</option>
                                                                        <option value="2005">2005</option>
                                                                        <option value="2004">2004</option>
                                                                        <option value="2003">2003</option>
                                                                        <option value="2002">2002</option>
                                                                        <option value="2001">2001</option>
                                                                        <option value="2000">2000</option>
                                                                        <option value="1999">1999</option>
                                                                        <option value="1998">1998</option>
                                                                        <option value="1997">1997</option>
                                                                        <option value="1996">1996</option>
                                                                        <option value="1995">1995</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                            
                                                        
                                                            <li>
                                                                <label>Localidad:</label>
                                                                <div class="automobile-banner-select">
                                                                    <select name="location" required>
                                                                        <option selected disable>Localidad</option>
                                                                        <?php foreach($localidades as $localidad) : ?>
                                                                            <option value="<?=$localidad?>"><?= $localidad ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                            <li class="banner-full-form">
                                                                <label class="banner-submit"><i class="icon-search"></i><input type="submit" name="bike-4" value="Cotizar"></label>
                                                            </li>
                                                        </ul>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// Slider \\-->
                
            </div>
            <!--// Main Banner \\-->
            
            
            <!--// Main Content \\-->
            
        <div  class="automobile-main-content">
                <div id="nosotros"></div>
            <!--// Main Section \\-->
            <div class="automobile-main-section automobile-about-full">
                <div class="container">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="automobile-fancy-title">
                                <h2>Quienes <span>Somos<span></h2>
                                <span>Servicio y Confianza.</span>
                                <p >Somos un equipo de asesores de seguros, con años en el mercado y venimos a hacer tu vida más fácil. Queremos acompañarte durante todo el camino para que tu experiencia sea la mejor de principio a fin. Tu satisfacción es nuestra prioridad.</p>
                                <br>
                                <h2 style="font-size: 18px">Por que <span>Elegirnos<span></h2>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    
                    <div class="row quienes-somos">
                        <div class="col-12 col-md-6">
                            <div class="automobile-about-services-list">
                                <ul>
                                    <li class="list-one lists">
                                        <i style="color:#6BAC40;" class="fas fa-donate"></i>
                                        <div class="automobile-about-list-text">
                                            <h4>Descuentos</h4>
                                            <p>Podés ahorrar hasta un 30% de descuento pagando con débito automático y un 15% adicional cada año que renueves con nosotros.</p>
                                        </div>
                                    </li>
                                    <li class="list-two lists">
                                        <i style="color:#6BAC40;" class="fas fa-user-friends"></i>
                                        <div class="automobile-about-list-text">
                                            <h4>Atención Personalizada</h4>
                                            <p>Queremos que tengas la mejor experiencia  por eso te dedicamos el tiempo que te mereces.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>                            
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="automobile-about-services-list">
                                <ul>
                                    <li class="list-one lists">
                                        <i style="color:#6BAC40;" class="far fa-handshake"></i>
                                        <div class="automobile-about-list-text">
                                            <h4>Confianza</h4>
                                            <p id="companias">Brindamos una experiencia clara y transparente, sin letra chica, por eso trabajamos con las mejores aseguradoras.</p>
                                        </div>
                                    </li>
                                    <li class="list-two lists">
                                        <i style="color:#6BAC40;" class="fab fa-envira"></i>
                                        <div class="automobile-about-list-text">
                                            <h4>100% Digital</h4>
                                            <p id="companias">Nos preocupamos por el medio ambiente, por eso trabajamos con un proceso 100% digital evitando el uso innecesario de papel.</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <!--// Main Section \\-->
            <h2 class="text-center" style="margin-bottom: 20px; color: #3d414e">Compañías con las que trabajamos</h2>
            <div id="contacto" class="automobile-footer-slider" style="height: 200px;">

                <div class="automobile-footer-slider-layer">
                    <figure>
                        <img src="https://www.cleverbroker.com.ar/extra-images/logo-zurich.jpg" alt="">
                    </figure>
                </div>
                <div class="automobile-footer-slider-layer">
                    <figure>
                        <img src="extra-images/logo-sancor.jpg" alt="">
                    </figure>
                </div>
                <div class="automobile-footer-slider-layer">
                    <figure>
                        <img src="extra-images/logo-allianz.jpg" alt="">
                    </figure>
                </div>
                <div class="automobile-footer-slider-layer">
                    <figure>
                        <img src="extra-images/logo-libra.jpg" alt="">
                    </figure>
                </div>
                <div class="automobile-footer-slider-layer">
                    <figure>
                        <img src="extra-images/logo-caledonia.jpg" alt="">
                    </figure>
                </div>
                <div class="automobile-footer-slider-layer">
                    <figure>
                        <img src="extra-images/logo-mercantil.jpg" alt="">
                    </figure>
                </div>  
            </div>

            <div class="automobile-main-section">
                <hr>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3>Contacto</h3>
                            <p>Nuestras oficinas de atención al público se encuentran en Av. Leandro N. Alem 592, piso 3, CABA, CP 1001.( MAPA )  Y nuestro horario de atención es de lunes a viernes de 10 a 18 hs.</p>
                            <p>Cel:+54(11)6184-7991/5888-6157</p>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3284.131815015448!2d-58.37269054938349!3d-34.60082818036419!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95a335333c265df9%3A0x189b9f2327f7e1ad!2sAv.+Leandro+N.+Alem+592%2C+C1001+CABA!5e0!3m2!1ses!2sar!4v1562706198190!5m2!1ses!2sar" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!--// Main Section \\-->

		</div>
		<!--// Main Content \\-->

        <?php include ('partials/footer.php'); ?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="https://api.whatsapp.com/send?phone=5491158886157&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n." class="float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
    </a>
    
	<?php include ('partials/scripts.php'); ?>
  </body>
  
</html>